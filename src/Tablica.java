/**
 * Created by PGSTESTER07 on 04.03.2017.
 */
public class Tablica {

    private char[] tablica;

    public Tablica(char znak, int liczbaElem)
    {
        this.tablica = new char[liczbaElem];
       WypenijTablice(znak);
    }

    private void WypenijTablice(char znak) {
        for(int i = 0; i<this.tablica.length;i++)
        {
            this.tablica[i] = znak;
        }
    }

    public void WyswietlTablice()
    {
        for(int i = 0;i<this.tablica.length;i++)
        {
            System.out.print(this.tablica[i]+" ");
        }
        System.out.println();
    }
}
