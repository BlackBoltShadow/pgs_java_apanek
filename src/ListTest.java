import java.util.LinkedList;
import java.util.List;

/**
 * Created by PGSTESTER07 on 04.03.2017.
 */
public class ListTest {
    private List<String> lista;

    public ListTest()
    {
        this.lista = new LinkedList<>();
    }

    public boolean DodajDoListy(String slowo)
    {
        if(slowo.length()<10)
        {
            this.lista.add(slowo);
            return true;
        }
        else
        {
            return false;
        }
    }

    public void WyswieltListe()
    {
        for(String slowo : this.lista)
        {
            System.out.println(slowo);
        }
    }
}
