/**
 * Created by PGSTESTER07 on 04.03.2017.
 */
public class Cwiczenie3 {

    private String slowo;

    public Cwiczenie3(String slowoPar)
    {
        this.slowo = slowoPar;
    }

    public char SprawdzSlowo()
    {
        if(this.slowo.length() % 2 == 0)
        {
            return this.slowo.charAt(this.slowo.length()-1);
        }
        else
        {
            return 'X';
        }
    }
}
