import javafx.scene.control.Tab;

/**
 * Created by PGSTESTER07 on 04.03.2017.
 */
public class MainKlas {

    public static void main(String[] args) {

        //Zadania z zajęć
        Test test1 = new Test("Pierwszy test");
        Test test2 = new Test("Drugi test");
        Test test3 = new Test("Trzeci test");

        Tablica tablica = new Tablica('*', 10);
        tablica.WyswietlTablice();

        Cwiczenie3 cw3 = new Cwiczenie3("LAMA");
        System.out.println(cw3.SprawdzSlowo());

        System.out.println(new Cwiczenie3("LAMANParz").SprawdzSlowo());

        ListTest listTest = new ListTest();
        listTest.DodajDoListy("Cospowyrzej10znakow");
        listTest.DodajDoListy("Lama");
        listTest.DodajDoListy("Test");
        listTest.DodajDoListy("Alpaka");
        listTest.WyswieltListe();
    }
}
